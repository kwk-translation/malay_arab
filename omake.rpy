﻿# TODO: Translation updated at 2018-06-29 08:45

translate malay_arab strings:

    # omake.rpy:86
    old "Opening song \"TAKE MY HEART\""
    new "Opening song \"TAKE MY HEART\""

    # omake.rpy:87
    old "Performed by Mew Nekohime\nLyrics by Max le Fou and Masaki Deguchi\nComposed and sequenced by Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"
    new "Performed by Mew Nekohime\nLyrics by Max le Fou and Masaki Deguchi\nComposed and sequenced by Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"

    # omake.rpy:93
    old "{b}Japanese lyrics:{/b}\n"
    new "{b}Japanese lyrics:{/b}\n"

    # omake.rpy:109
    old "{b}Romaji:{/b}\n"
    new "{b}Romaji:{/b}\n"

    # omake.rpy:125
    old "{b}Translation:{/b}\n"
    new "{b}Translation:{/b}\n"

    # omake.rpy:126
    old "When you take my hand, I feel I could fly"
    new "When you take my hand, I feel I could fly"

    # omake.rpy:127
    old "When I dive into your eyes, I feel I could drown in happiness\n"
    new "When I dive into your eyes, I feel I could drown in happiness\n"

    # omake.rpy:128
    old "We sure look different"
    new "We sure look different"

    # omake.rpy:129
    old "But despite that, my heart beats loud\n"
    new "But despite that, my heart beats loud\n"

    # omake.rpy:130
    old "A boy and a girl"
    new "A boy and a girl"

    # omake.rpy:131
    old "I'm just a human"
    new "I'm just a human"

    # omake.rpy:132
    old "I hope you don't mind"
    new "I hope you don't mind"

    # omake.rpy:133
    old "I can't control my feelings"
    new "I can't control my feelings"

    # omake.rpy:134
    old "Come to me, take my heart"
    new "Come to me, take my heart"

    # omake.rpy:135
    old "I will devote myself to you"
    new "I will devote myself to you"

    # omake.rpy:136
    old "No matter what, I love you"
    new "No matter what, I love you"

    # omake.rpy:157
    old "{b}D.O.B.:{/b} 1978/09/29\n"
    new "{b}D.O.B.:{/b} 1978/09/29\n"

    # omake.rpy:158
    old "{b}P.O.B.:{/b} Shinjuku, Tokyo\n"
    new "{b}P.O.B.:{/b} Shinjuku, Tokyo\n"

    # omake.rpy:159
    old "{b}Height:{/b} 5.4ft\n"
    new "{b}Height:{/b} 5.4ft\n"

    # omake.rpy:160
    old "{b}Weight:{/b} 136 pounds\n"
    new "{b}Weight:{/b} 136 pounds\n"

    # omake.rpy:161
    old "{b}Measurements:{/b} 74-64-83\n"
    new "{b}Measurements:{/b} 74-64-83\n"

    # omake.rpy:162
    old "{b}Blood type:{/b} A\n"
    new "{b}Blood type:{/b} A\n"

    # omake.rpy:163
    old "{b}Favourite manga:{/b} High School Samurai\n"
    new "{b}Favourite manga:{/b} High School Samurai\n"

    # omake.rpy:164
    old "{b}Favourite videogame:{/b} Lead of Fighters ‘96\n"
    new "{b}Favourite videogame:{/b} Lead of Fighters ‘96\n"

    # omake.rpy:165
    old "{b}Favourite food:{/b} American hamburgers\n"
    new "{b}Favourite food:{/b} American hamburgers\n"

    # omake.rpy:166
    old "A young boy from Tokyo who has just moved to the village. At first, he thinks he's going to miss the urban life he knew before. But meeting Sakura will quickly change his mind...\nHe is a nice guy, determined, and sometimes a little bit crazy. He likes computers, mangas and loves to have fun with his friends. He is not afraid to face problems, especially when the sake of his friends is involved. He is quite uncertain on big decisions so he usually lets his instinct (or the player!) leading his decisions most of the time...\nHe got an older sister that is married and still lives in Tokyo."
    new "A young boy from Tokyo who has just moved to the village. At first, he thinks he's going to miss the urban life he knew before. But meeting Sakura will quickly change his mind...\nHe is a nice guy, determined, and sometimes a little bit crazy. He likes computers, mangas and loves to have fun with his friends. He is not afraid to face problems, especially when the sake of his friends is involved. He is quite uncertain on big decisions so he usually lets his instinct (or the player!) leading his decisions most of the time...\nHe got an older sister that is married and still lives in Tokyo."

    # omake.rpy:184
    old "{b}D.O.B.:{/b} 1979/02/28\n"
    new "{b}D.O.B.:{/b} 1979/02/28\n"

    # omake.rpy:185
    old "{b}P.O.B.:{/b} Kameoka, Kyoto\n"
    new "{b}P.O.B.:{/b} Kameoka, Kyoto\n"

    # omake.rpy:186
    old "{b}Height:{/b} 5.1ft\n"
    new "{b}Height:{/b} 5.1ft\n"

    # omake.rpy:187
    old "{b}Weight:{/b} 121 pounds\n"
    new "{b}Weight:{/b} 121 pounds\n"

    # omake.rpy:188
    old "{b}Measurements:{/b} Unknown\n"
    new "{b}Measurements:{/b} Unknown\n"

    # omake.rpy:189
    old "{b}Blood type:{/b} AB\n"
    new "{b}Blood type:{/b} AB\n"

    # omake.rpy:190
    old "{b}Favourite manga:{/b} Uchuu Tenshi Moechan\n"
    new "{b}Favourite manga:{/b} Uchuu Tenshi Moechan\n"

    # omake.rpy:191
    old "{b}Favourite videogame:{/b} Taiko no Masuta EX 4’\n"
    new "{b}Favourite videogame:{/b} Taiko no Masuta EX 4’\n"

    # omake.rpy:192
    old "{b}Favourite food:{/b} Beef yakitori\n"
    new "{b}Favourite food:{/b} Beef yakitori\n"

    # omake.rpy:193
    old "Sakura is a member of the school's manga club and she has a very deep secret that makes of her a mysterious girl...\nShe is very shy but incredibly pretty. She was the idol of the school until a strange rumor about her started to spread. She likes classical music and plays violin sometimes in the night at her window..."
    new "Sakura is a member of the school's manga club and she has a very deep secret that makes of her a mysterious girl...\nShe is very shy but incredibly pretty. She was the idol of the school until a strange rumor about her started to spread. She likes classical music and plays violin sometimes in the night at her window..."

    # omake.rpy:216
    old "{b}D.O.B.:{/b} 1979/08/05\n"
    new "{b}D.O.B.:{/b} 1979/08/05\n"

    # omake.rpy:217
    old "{b}P.O.B.:{/b} The Village, Osaka\n"
    new "{b}P.O.B.:{/b} The Village, Osaka\n"

    # omake.rpy:218
    old "{b}Height:{/b} 5ft\n"
    new "{b}Height:{/b} 5ft\n"

    # omake.rpy:219
    old "{b}Weight:{/b} 110 pounds\n"
    new "{b}Weight:{/b} 110 pounds\n"

    # omake.rpy:220
    old "{b}Measurements:{/b} 92-64-87\n"
    new "{b}Measurements:{/b} 92-64-87\n"

    # omake.rpy:221
    old "{b}Blood type:{/b} O\n"
    new "{b}Blood type:{/b} O\n"

    # omake.rpy:222
    old "{b}Favourite manga:{/b} Rosario Maiden\n"
    new "{b}Favourite manga:{/b} Rosario Maiden\n"

    # omake.rpy:223
    old "{b}Favourite videogame:{/b} Super Musashi Galaxy Fight\n"
    new "{b}Favourite videogame:{/b} Super Musashi Galaxy Fight\n"

    # omake.rpy:224
    old "{b}Favourite food:{/b} Takoyaki\n"
    new "{b}Favourite food:{/b} Takoyaki\n"

    # omake.rpy:225
    old "Rika is the founder of the manga club.\nShe got very bad experiences with boys and she sees them as perverts since then. Rika cosplays as a hobby and her best and favourite cosplay is the heroine of the Domoco-chan anime. She have strange eyes minnows that makes every boys dreamy. She speaks in the Kansai dialect like most of the people originating from the Village.\nShe secretly have a little crush on Sakura..."
    new "Rika is the founder of the manga club.\nShe got very bad experiences with boys and she sees them as perverts since then. Rika cosplays as a hobby and her best and favourite cosplay is the heroine of the Domoco-chan anime. She have strange eyes minnows that makes every boys dreamy. She speaks in the Kansai dialect like most of the people originating from the Village.\nShe secretly have a little crush on Sakura..."

    # omake.rpy:248
    old "{b}D.O.B.:{/b} 1980/10/11\n"
    new "{b}D.O.B.:{/b} 1980/10/11\n"

    # omake.rpy:249
    old "{b}P.O.B.:{/b} Ginoza, Okinawa\n"
    new "{b}P.O.B.:{/b} Ginoza, Okinawa\n"

    # omake.rpy:250
    old "{b}Height:{/b} 4.5ft\n"
    new "{b}Height:{/b} 4.5ft\n"

    # omake.rpy:251
    old "{b}Weight:{/b} 99 pounds\n"
    new "{b}Weight:{/b} 99 pounds\n"

    # omake.rpy:252
    old "{b}Measurements:{/b} 71-51-74\n"
    new "{b}Measurements:{/b} 71-51-74\n"

    # omake.rpy:253
    old "{b}Blood type:{/b} B\n"
    new "{b}Blood type:{/b} B\n"

    # omake.rpy:254
    old "{b}Favourite manga:{/b} Nanda no Ryu\n"
    new "{b}Favourite manga:{/b} Nanda no Ryu\n"

    # omake.rpy:255
    old "{b}Favourite videogame:{/b} Pika Pika Rocket\n"
    new "{b}Favourite videogame:{/b} Pika Pika Rocket\n"

    # omake.rpy:256
    old "{b}Favourite food:{/b} Chanpuruu\n"
    new "{b}Favourite food:{/b} Chanpuruu\n"

    # omake.rpy:257
    old "Nanami lives alone with her big brother Toshio after their parents disappeared.\nShe's a quiet introvert girl at first glance, but once she's with her friends, she's a cute energy bomb. She has a natural talent for videogames, which made of her the champion of the Osaka prefecture in numerous videogames."
    new "Nanami lives alone with her big brother Toshio after their parents disappeared.\nShe's a quiet introvert girl at first glance, but once she's with her friends, she's a cute energy bomb. She has a natural talent for videogames, which made of her the champion of the Osaka prefecture in numerous videogames."
# TODO: Translation updated at 2019-04-27 11:12

translate malay_arab strings:

    # omake.rpy:278
    old "Chapter 1"
    new "Chapter 1"

    # omake.rpy:278
    old "Complete the first chapter"
    new "Complete the first chapter"

    # omake.rpy:278
    old "Chapter 2"
    new "Chapter 2"

    # omake.rpy:278
    old "Complete the second chapter"
    new "Complete the second chapter"

    # omake.rpy:278
    old "Chapter 3"
    new "Chapter 3"

    # omake.rpy:278
    old "Complete the third chapter"
    new "Complete the third chapter"

    # omake.rpy:278
    old "Chapter 4"
    new "Chapter 4"

    # omake.rpy:278
    old "Complete the fourth chapter"
    new "Complete the fourth chapter"

    # omake.rpy:278
    old "Chapter 5"
    new "Chapter 5"

    # omake.rpy:278
    old "Complete the fifth chapter"
    new "Complete the fifth chapter"

    # omake.rpy:278
    old "Finally together"
    new "Finally together"

    # omake.rpy:278
    old "Finish the game for the first time"
    new "Finish the game for the first time"

    # omake.rpy:278
    old "The perfume of rice fields"
    new "The perfume of rice fields"

    # omake.rpy:278
    old "Get a kiss from Sakura"
    new "Get a kiss from Sakura"

    # omake.rpy:278
    old "It's not that I like you, baka!"
    new "It's not that I like you, baka!"

    # omake.rpy:278
    old "Get a kiss from Rika"
    new "Get a kiss from Rika"

    # omake.rpy:278
    old "A new kind of game"
    new "A new kind of game"

    # omake.rpy:278
    old "Get a kiss from Nanami"
    new "Get a kiss from Nanami"

    # omake.rpy:278
    old "It's a trap!"
    new "It's a trap!"

    # omake.rpy:278
    old "Find the truth about Sakura"
    new "Find the truth about Sakura"

    # omake.rpy:278
    old "Good guy"
    new "Good guy"

    # omake.rpy:278
    old "Tell Sakura the truth about the yukata"
    new "Tell Sakura the truth about the yukata"

    # omake.rpy:278
    old "It's all about the Pentiums, baby"
    new "It's all about the Pentiums, baby"

    # omake.rpy:278
    old "Choose to game at the beginning of the game."
    new "Choose to game at the beginning of the game."

    # omake.rpy:278
    old "She got me!"
    new "She got me!"

    # omake.rpy:278
    old "Help Rika with the festival"
    new "Help Rika with the festival"

    # omake.rpy:278
    old "Grope!"
    new "Grope!"

    # omake.rpy:278
    old "Grope Rika (accidentally)"
    new "Grope Rika (accidentally)"

    # omake.rpy:278
    old "A good prank"
    new "A good prank"

    # omake.rpy:278
    old "Prank your friends with Nanami"
    new "Prank your friends with Nanami"

    # omake.rpy:278
    old "I'm not little!"
    new "I'm not little!"

    # omake.rpy:278
    old "Help Sakura tell the truth to Nanami"
    new "Help Sakura tell the truth to Nanami"

    # omake.rpy:278
    old "Big change of life"
    new "Big change of life"

    # omake.rpy:278
    old "Complete Sakura's route"
    new "Complete Sakura's route"

    # omake.rpy:278
    old "City Rat"
    new "City Rat"

    # omake.rpy:278
    old "Complete Rika's route"
    new "Complete Rika's route"

    # omake.rpy:278
    old "That new girl"
    new "That new girl"

    # omake.rpy:278
    old "Complete Nanami's route"
    new "Complete Nanami's route"

    # omake.rpy:278
    old "Knee-Deep into the 34D"
    new "Knee-Deep into the 34D"

    # omake.rpy:278
    old "Find the secret code in the game"
    new "Find the secret code in the game"

