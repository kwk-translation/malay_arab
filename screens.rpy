﻿# TODO: Translation updated at 2019-04-23 11:19

translate malay_arab strings:

    # screens.rpy:253
    old "Back"
    new "Back"

    # screens.rpy:254
    old "History"
    new "History"

    # screens.rpy:255
    old "Skip"
    new "Skip"

    # screens.rpy:256
    old "Auto"
    new "Auto"

    # screens.rpy:257
    old "Save"
    new "Save"

    # screens.rpy:258
    old "Q.save"
    new "Q.save"

    # screens.rpy:259
    old "Q.load"
    new "Q.load"

    # screens.rpy:260
    old "Settings"
    new "Settings"

    # screens.rpy:302
    old "New game"
    new "New game"

    # screens.rpy:311
    old "Update Available!"
    new "Update Available!"

    # screens.rpy:322
    old "Load"
    new "Load"

    # screens.rpy:325
    old "Bonus"
    new "Bonus"

    # screens.rpy:331
    old "End Replay"
    new "End Replay"

    # screens.rpy:335
    old "Main menu"
    new "Main menu"

    # screens.rpy:337
    old "About"
    new "About"

    # screens.rpy:341
    old "Please donate!"
    new "Please donate!"

    # screens.rpy:344
    old "Help"
    new "Help"

    # screens.rpy:348
    old "Quit"
    new "Quit"

    # screens.rpy:577
    old "Version [config.version!t]\n"
    new "Version [config.version!t]\n"

    # screens.rpy:583
    old "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"
    new "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"

    # screens.rpy:623
    old "Page {}"
    new "Page {}"

    # screens.rpy:623
    old "Automatic saves"
    new "Automatic saves"

    # screens.rpy:623
    old "Quick saves"
    new "Quick saves"

    # screens.rpy:665
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %B %d %Y, %H:%M"

    # screens.rpy:665
    old "empty slot"
    new "empty slot"

    # screens.rpy:682
    old "<"
    new "<"

    # screens.rpy:685
    old "{#auto_page}A"
    new "{#auto_page}A"

    # screens.rpy:688
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # screens.rpy:694
    old ">"
    new ">"

    # screens.rpy:756
    old "Display"
    new "Display"

    # screens.rpy:757
    old "Windowed"
    new "Windowed"

    # screens.rpy:758
    old "Fullscreen"
    new "Fullscreen"

    # screens.rpy:762
    old "Rollback Side"
    new "Rollback Side"

    # screens.rpy:763
    old "Disable"
    new "Disable"

    # screens.rpy:764
    old "Left"
    new "Left"

    # screens.rpy:765
    old "Right"
    new "Right"

    # screens.rpy:770
    old "Unseen Text"
    new "Unseen Text"

    # screens.rpy:771
    old "After choices"
    new "After choices"

    # screens.rpy:772
    old "Transitions"
    new "Transitions"

    # screens.rpy:782
    old "Language"
    new "Language"

    # screens.rpy:812
    old "Text speed"
    new "Text speed"

    # screens.rpy:816
    old "Auto forward"
    new "Auto forward"

    # screens.rpy:823
    old "Music volume"
    new "Music volume"

    # screens.rpy:830
    old "Sound volume"
    new "Sound volume"

    # screens.rpy:836
    old "Test"
    new "Test"

    # screens.rpy:840
    old "Voice volume"
    new "Voice volume"

    # screens.rpy:851
    old "Mute All"
    new "Mute All"

    # screens.rpy:970
    old "The dialogue history is empty."
    new "The dialogue history is empty."

    # screens.rpy:1035
    old "Keyboard"
    new "Keyboard"

    # screens.rpy:1036
    old "Mouse"
    new "Mouse"

    # screens.rpy:1039
    old "Gamepad"
    new "Gamepad"

    # screens.rpy:1052
    old "Enter"
    new "Enter"

    # screens.rpy:1053
    old "Advances dialogue and activates the interface."
    new "Advances dialogue and activates the interface."

    # screens.rpy:1056
    old "Space"
    new "Space"

    # screens.rpy:1057
    old "Advances dialogue without selecting choices."
    new "Advances dialogue without selecting choices."

    # screens.rpy:1060
    old "Arrow Keys"
    new "Arrow Keys"

    # screens.rpy:1061
    old "Navigate the interface."
    new "Navigate the interface."

    # screens.rpy:1064
    old "Escape"
    new "Escape"

    # screens.rpy:1065
    old "Accesses the game menu."
    new "Accesses the game menu."

    # screens.rpy:1068
    old "Ctrl"
    new "Ctrl"

    # screens.rpy:1069
    old "Skips dialogue while held down."
    new "Skips dialogue while held down."

    # screens.rpy:1072
    old "Tab"
    new "Tab"

    # screens.rpy:1073
    old "Toggles dialogue skipping."
    new "Toggles dialogue skipping."

    # screens.rpy:1076
    old "Page Up"
    new "Page Up"

    # screens.rpy:1077
    old "Rolls back to earlier dialogue."
    new "Rolls back to earlier dialogue."

    # screens.rpy:1080
    old "Page Down"
    new "Page Down"

    # screens.rpy:1081
    old "Rolls forward to later dialogue."
    new "Rolls forward to later dialogue."

    # screens.rpy:1085
    old "Hides the user interface."
    new "Hides the user interface."

    # screens.rpy:1089
    old "Takes a screenshot."
    new "Takes a screenshot."

    # screens.rpy:1093
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # screens.rpy:1099
    old "Left Click"
    new "Left Click"

    # screens.rpy:1103
    old "Middle Click"
    new "Middle Click"

    # screens.rpy:1107
    old "Right Click"
    new "Right Click"

    # screens.rpy:1111
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Mouse Wheel Up\nClick Rollback Side"

    # screens.rpy:1115
    old "Mouse Wheel Down"
    new "Mouse Wheel Down"

    # screens.rpy:1122
    old "Right Trigger\nA/Bottom Button"
    new "Right Trigger\nA/Bottom Button"

    # screens.rpy:1126
    old "Left Trigger\nLeft Shoulder"
    new "Left Trigger\nLeft Shoulder"

    # screens.rpy:1130
    old "Right Shoulder"
    new "Right Shoulder"

    # screens.rpy:1134
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # screens.rpy:1138
    old "Start, Guide"
    new "Start, Guide"

    # screens.rpy:1142
    old "Y/Top Button"
    new "Y/Top Button"

    # screens.rpy:1145
    old "Calibrate"
    new "Calibrate"

    # screens.rpy:1210
    old "Yes"
    new "Yes"

    # screens.rpy:1211
    old "No"
    new "No"

    # screens.rpy:1257
    old "Skipping"
    new "Skipping"

    # screens.rpy:1478
    old "Menu"
    new "Menu"

    # screens.rpy:1532
    old "Musics and pictures"
    new "Musics and pictures"

    # screens.rpy:1535
    old "Picture gallery"
    new "Picture gallery"

    # screens.rpy:1539
    old "Music player"
    new "Music player"

    # screens.rpy:1541
    old "Opening song lyrics"
    new "Opening song lyrics"

    # screens.rpy:1545
    old "Characters profiles"
    new "Characters profiles"

    # screens.rpy:1566
    old "Bonus chapters"
    new "Bonus chapters"

    # screens.rpy:1569
    old "1 - A casual day at the club"
    new "1 - A casual day at the club"

    # screens.rpy:1574
    old "2 - Questioning sexuality (Sakura's route)"
    new "2 - Questioning sexuality (Sakura's route)"

    # screens.rpy:1579
    old "3 - Headline news"
    new "3 - Headline news"

    # screens.rpy:1584
    old "4a - A picnic at the summer (Sakura's route)"
    new "4a - A picnic at the summer (Sakura's route)"

    # screens.rpy:1589
    old "4b - A picnic at the summer (Rika's route)"
    new "4b - A picnic at the summer (Rika's route)"

    # screens.rpy:1594
    old "4c - A picnic at the summer (Nanami's route)"
    new "4c - A picnic at the summer (Nanami's route)"

    # screens.rpy:1611
    old "Max le Fou - Taichi's Theme"
    new "Max le Fou - Taichi's Theme"

    # screens.rpy:1613
    old "Max le Fou - Sakura's Waltz"
    new "Max le Fou - Sakura's Waltz"

    # screens.rpy:1615
    old "Max le Fou - Rika's theme"
    new "Max le Fou - Rika's theme"

    # screens.rpy:1617
    old "Max le Fou - Of Bytes and Sanshin"
    new "Max le Fou - Of Bytes and Sanshin"

    # screens.rpy:1619
    old "Max le Fou - Time for School"
    new "Max le Fou - Time for School"

    # screens.rpy:1621
    old "Max le Fou - Sakura's secret"
    new "Max le Fou - Sakura's secret"

    # screens.rpy:1623
    old "Max le Fou - I think I'm in love"
    new "Max le Fou - I think I'm in love"

    # screens.rpy:1625
    old "Max le Fou - Darkness of the Village"
    new "Max le Fou - Darkness of the Village"

    # screens.rpy:1627
    old "Max le Fou - Love always win"
    new "Max le Fou - Love always win"

    # screens.rpy:1629
    old "Max le Fou - Ondo"
    new "Max le Fou - Ondo"

    # screens.rpy:1631
    old "Max le Fou - Happily Ever After"
    new "Max le Fou - Happily Ever After"

    # screens.rpy:1633
    old "J.S. Bach - Air Orchestral suite #3"
    new "J.S. Bach - Air Orchestral suite #3"

    # screens.rpy:1635
    old "Mew Nekohime - TAKE MY HEART (TV Size)"
    new "Mew Nekohime - TAKE MY HEART (TV Size)"

# TODO: Translation updated at 2019-04-27 11:12

translate malay_arab strings:

    # screens.rpy:1552
    old "Achievements"
    new "Achievements"

